Yeargan & Kert, LLC is one of the top DUI law firms in Atlanta, GA. With over 50 years of experience, our talented DUI attorneys have handled all kinds of criminal cases. 

Jim Yeargans dedication to those facing DUI charges has earned him the nickname DUI Jim. Throughout the years, he has personally handled hundreds of DUI cases ranging from the most simple offenses to the most complicated charges. 

Jims commitment to excellence has earned him numerous awards, including the prestigious AV Preeminent Rating from Martindale-Hubbell. Jim is also a member of numerous associations, including the American Bar Association, the American Trial Lawyers Association, the Atlanta Bar Association, the National Association of Criminal Defense Attorneys, and more. 

Julie Kert has a very distinguished career as a criminal defense attorney. In addition to having personally handled over 1,000 DUI cases herself, she has also first-chaired over 1,000 bench trials and first chaired over 80 jury trials. This experience makes Julie one of the most experienced DUI attorneys in the state of Georgia, and definitely someone you would want on your side. 

The attorneys at Yeargan & Kert, LLC handle all kinds of DUI cases, including first-time DUI, second-time DUI, third-time DUI or habitual violator DUI, DUI of drugs, misdemeanor DUI, felony DUI, and many more. 

We understand that facing a DUI can be very scary. Thats why we offer a free consultation 24/7. Call our office today and schedule a consultation with one of our attorneys who will be able to discuss your case with you. No fees and no questions asked. Call today and start protecting your future with Yeargan & Kert, LLC.

Address: 1170 Peachtree Street, Suite 1200, Atlanta, GA 30309, USA

Phone: (404) 467-1747

Website: https://www.atlantaduilawyer.com

